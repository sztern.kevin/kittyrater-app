import main

def test_get_cat_rate():
    c = main.app.test_client()
    ret = c.get("/")
    assert b"this cat is rated 10/10!" in ret.data
